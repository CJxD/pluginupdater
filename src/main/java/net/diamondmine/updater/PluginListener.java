package net.diamondmine.updater;

import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;

/**
 * Event handlers.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class PluginListener implements Listener {
    private final PluginUpdater plugin;

    /**
     * Constructor.
     * 
     * @param instance
     *            PluginUpdater class.
     */
    public PluginListener(final PluginUpdater instance) {
        plugin = instance;
    }

    /**
     * Adds any new plugins to the configuration file.
     * 
     * @param event
     *            PluginEnableEvent event.
     * @since 1.0.0
     */
    public final void onPluginEnable(final PluginEnableEvent event) {
        plugin.checkConfig();
    }

}
