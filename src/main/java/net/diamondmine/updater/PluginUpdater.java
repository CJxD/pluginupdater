package net.diamondmine.updater;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of PluginUpdater.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class PluginUpdater extends JavaPlugin {
    private final PluginListener pluginListener = new PluginListener(this);
    public static final Logger logger = Logger.getLogger("Minecraft");
    public FileConfiguration config;

    /**
     * Updater disabled.
     */
    @Override
    public final void onDisable() {
        log("Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Updater enabled.
     */
    @Override
    public final void onEnable() {
        // Settings
        config = getConfig();
        checkConfig();

        // Cache
        new File(getDataFolder() + File.separator + "cache").mkdir();
        getServer().getScheduler().scheduleAsyncRepeatingTask(this, new UpdaterTask(this), 60L, (config.getLong("options.autoupdate.interval") * 60) * 20);

        log("Version " + getDescription().getVersion() + " enabled", "info");

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(pluginListener, this);
    }

    @Override
    public final boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, final String[] args) {
        if (cmd.getName().equalsIgnoreCase("update")) {
            if (!sender.hasPermission("pluginupdater.update")) {
                sender.sendMessage(ChatColor.RED + "You do not have the permission to do this");
                return true;
            }

            if (args.length == 0) {
                sender.sendMessage(ChatColor.GOLD + "You must specify at least one plugin");
                return true;
            }

            for (String arg : args) {
                try {
                    updatePlugin(arg, sender);
                } catch (Exception e) {
                    log("Unable to update plugin '" + arg + "': " + e.getLocalizedMessage(), "severe");
                }
            }
            return true;
        } else if (cmd.getName().equalsIgnoreCase("install")) {
            if (!sender.hasPermission("pluginupdater.install")) {
                sender.sendMessage(ChatColor.RED + "You do not have the permission to do this");
                return true;
            }

            if (args.length == 0 || args.length > 1) {
                sender.sendMessage(ChatColor.GOLD + "You must specify one plugin");
                return true;
            }

            for (String arg : args) {
                try {
                    installPlugin(arg, sender);
                } catch (Exception e) {
                    log("Unable to update plugin '" + arg + "': " + e.getLocalizedMessage(), "severe");
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Updates a plugin.
     * 
     * @param plugin
     *            The name of the plugin.
     * @param sender
     *            CommandSender. Can be null.
     * @throws MalformedURLException
     *             Invalid URL.
     * @since 1.0.0
     */
    public final void updatePlugin(final String plugin, final CommandSender sender) throws MalformedURLException {
        String url = "http://dev.bukkit.org/server-mods/" + plugin.toLowerCase() + "/";
        if (config.contains("plugins." + plugin.toLowerCase() + ".location")) {
            url = config.getString("plugins." + plugin.toLowerCase() + ".location");
        }
        Runnable updater = new Updater(this, url, sender, true);
        new Thread(updater).start();
    }

    /**
     * Installs a plugin.
     * 
     * @param plugin
     *            The name of the plugin.
     * @param sender
     *            CommandSender. Can be null.
     * @throws MalformedURLException
     *             Invalid URL.
     * @since 1.0.0
     */
    public final void installPlugin(final String plugin, final CommandSender sender) throws MalformedURLException {
        String url = "http://dev.bukkit.org/server-mods/" + plugin.toLowerCase() + "/";
        if (config.contains("plugins." + plugin.toLowerCase() + ".location")) {
            url = config.getString("plugins." + plugin.toLowerCase() + ".location");
        }
        Runnable updater = new Updater(this, url, sender, false);
        new Thread(updater).start();
    }

    /**
     * Sets default configuration up if there isn't a configuration file
     * present.
     * 
     * @since 1.0.0
     */
    protected final void checkConfig() {
        config.options().copyDefaults(true);
        config.addDefault("options.autoupdate.interval", 360);
        config.addDefault("options.autoupdate.cacheage", 1440);
        config.addDefault("plugins", null);
        for (Plugin pl : getServer().getPluginManager().getPlugins()) {
            Map<String, String> defaults = new HashMap<String, String>();
            defaults.put("location", "http://dev.bukkit.org/server-mods/" + pl.getName().toLowerCase() + "/");
            defaults.put("autoupdate", "off");
            config.addDefault("plugins." + pl.getName().toLowerCase(), defaults);
        }
        config.options().copyDefaults(true);
        saveConfig();
    }

    /**
     * Adds option to plugin in configuration.
     * 
     * @param pl
     *            The name of the plugin.
     * @param name
     *            Option name.
     * @param value
     *            Value of option.
     * @since 1.0.0
     */
    public final void insertOption(final String pl, final String name, final String value) {
        if (!config.contains("plugins." + pl.toLowerCase())) {
            Map<String, String> defaults = new HashMap<String, String>();
            defaults.put("location", "http://dev.bukkit.org/server-mods/" + pl.toLowerCase() + "/");
            defaults.put("autoupdate", "false");
            config.addDefault("plugins." + pl.toLowerCase(), defaults);
        }

        config.set("plugins." + pl.toLowerCase() + "." + name, value);
        saveOptions();
    }

    /**
     * Saves configuration options.
     * 
     * @since 1.0.0
     */
    public final void saveOptions() {
        File conf = new File("plugins/PluginUpdater/config.yml");
        try {
            config.save(conf);
        } catch (IOException e) {
            return;
        }
    }

    /**
     * 
     * @param pl
     *            The name of the plugin.
     * @param name
     *            Option name.
     * @return string MD5 of last used Zip file.
     */
    public final String getZipMD5(final String pl, final String name) {
        return config.getString("plugins." + pl.toLowerCase() + "." + name);
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.0
     */
    public static void log(final String s, final String type) {
        String message = "[PluginUpdater] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send.
     */
    public static void log(final String s) {
        log(s, "info");
    }
}
