package net.diamondmine.updater.debugger;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/*
 * Copyright (c) 2009-2010, i Data Connect!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
public class ErrorOutputStream extends FilterOutputStream {
    private int width = 72;
    private int pos;
    private int tuple;
    private int count;
    private boolean encoding;
    private boolean useSpaceCompression;

    public ErrorOutputStream(final OutputStream out) {
        super(out);
    }

    public ErrorOutputStream(final OutputStream out, final boolean useSpaceCompression) {
        this(out);
        this.useSpaceCompression = useSpaceCompression;
    }

    public ErrorOutputStream(final OutputStream out, final int width, final boolean useSpaceCompression) {
        this(out);
        this.width = width;
        this.useSpaceCompression = useSpaceCompression;
    }

    private void startEncoding() throws IOException {
        out.write('<');
        out.write('~');
        pos = 2;
        encoding = true;
    }

    @Override
    public void write(final int b) throws IOException {
        if (!encoding) {
            startEncoding();
        }

        switch (count++) {
        case 0:
            tuple |= ((b & 0xff) << 24);
            break;
        case 1:
            tuple |= ((b & 0xff) << 16);
            break;
        case 2:
            tuple |= ((b & 0xff) << 8);
            break;
        case 3:
            tuple |= (b & 0xff);
            if (tuple == 0) {
                out.write('z');
                if (pos++ >= width) {
                    pos = 0;
                    out.write('\r');
                    out.write('\n');
                }
            } else if (useSpaceCompression && (tuple == 0x20202020)) {
                out.write('y');
                if (pos++ >= width) {
                    pos = 0;
                    out.write('\r');
                    out.write('\n');
                }
            } else {
                encode(tuple, count);
            }
            tuple = 0;
            count = 0;
            break;
        }
    }

    public void writeUnencoded(final int b) throws IOException {
        super.write(b);
    }

    public void writeUnencoded(final byte[] b) throws IOException {
        writeUnencoded(b, 0, b.length);
    }

    public void writeUnencoded(final byte[] b, final int off, final int len) throws IOException {
        for (int i = 0; i < len; i++) {
            writeUnencoded(b[off + i]);
        }
    }

    private void encode(final int tuple, final int count) throws IOException {
        int i = 5;
        byte[] buf = new byte[5];
        short bufPos = 0;

        long longTuple = 0 | (tuple & 0xffffffffL);

        do {
            buf[bufPos++] = (byte) (longTuple % 85);
            longTuple /= 85;
        } while (--i > 0);

        i = count;
        do {
            out.write(buf[--bufPos] + '!');
            if (pos++ >= width) {
                pos = 0;
                out.write('\r');
                out.write('\n');
            }
        } while (i-- > 0);
    }

    @Override
    public void flush() throws IOException {
        // Add padding if required.
        if (encoding) {
            if (count > 0) {
                encode(tuple, count);
            }
            if (pos + 2 > width) {
                out.write('\r');
                out.write('\n');
            }

            out.write('~');
            out.write('>');
            out.write('\r');
            out.write('\n');

            encoding = false;
            tuple = count = 0;
        }

        super.flush();
    }

}
