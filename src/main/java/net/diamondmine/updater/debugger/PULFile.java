package net.diamondmine.updater.debugger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.io.Serializable;

import net.diamondmine.updater.PluginUpdater;

/**
 * PUL file object.
 * 
 * @author Jon la Cour
 * @since 1.0.0
 */
public class PULFile implements Serializable {
    private static final long serialVersionUID = 4464361982460347554L;
    public String name = String.valueOf(System.currentTimeMillis());
    public StackTraceElement[] stack;
    public String exceptionMessage;

    /**
     * Constructor.
     * 
     * @param stackTrace
     *            Exception stacktrace.
     * @param message
     *            Exception message.
     * @since 1.0.0
     */
    public PULFile(final StackTraceElement[] stackTrace, final String message) {
        stack = stackTrace;
        exceptionMessage = message;
    }

    /**
     * Saves a PUC file.
     * 
     * @since 1.0.0
     */
    public final void save() {
        try {
            String separator = File.separator;
            PrintWriter pul = new PrintWriter("plugins" + separator + "PluginUpdater" + separator + "log" + separator + name + ".pul");
            String error = exceptionMessage + "\n";
            for (StackTraceElement line : stack) {
                error += line.toString() + "\n";
            }
            ByteArrayOutputStream outByte = new ByteArrayOutputStream();
            ErrorOutputStream outAscii = new ErrorOutputStream(outByte);
            try {
                outAscii.write(error.getBytes());
                outAscii.flush();
            } catch (Exception e) {
                PluginUpdater.log("Error generating log file " + name + ".pul.", "warning");
            }
            pul.print(outByte.toString("ascii"));
            pul.close();
            outByte.close();
            outAscii.close();
        } catch (Exception e) {
            PluginUpdater.log("Error saving log file " + name + ".pul.", "warning");
        }
    }
}
