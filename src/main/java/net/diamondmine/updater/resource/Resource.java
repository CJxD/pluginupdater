package net.diamondmine.updater.resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import net.diamondmine.updater.PluginUpdater;

import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;

/**
 * @author Jon la Cour
 * @version 1.0.0
 */
public abstract class Resource {
    public String name;
    public String type;
    public URL url;
    public boolean success = false;

    /**
     * @param url
     *            URL of resource.
     * @param type
     *            Type of resource (jar, zip, or rar).
     * @param name
     *            Name of resource.
     * @since 1.0.0
     */
    public Resource(final URL url, final String type, final String name) {
        this.url = url;
        this.type = type.toLowerCase();
        this.name = name;
    }

    /**
     * Downloads the resource.
     * 
     * @throws IOException
     *             Error downloading file.
     * @return boolean based on success of download.
     * @since 1.0.0
     */
    public final boolean download() throws IOException {
        if (url == null) {
            return false;
        }

        ReadableByteChannel channel = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream("plugins" + File.separator + name + "." + type);
        fos.getChannel().transferFrom(channel, 0, 1 << 24);
        fos.close();

        try {
            process();
        } catch (Exception e) {
            PluginUpdater.log("Error processing " + type + " resource: " + e.getMessage(), "warning");
            return false;
        }

        try {
            if (valid()) {
                success = true;
            }
        } catch (Exception e) {
            PluginUpdater.log("Error verifying " + type + " resource: " + e.getMessage(), "warning");
            return false;
        }
        return true;
    }

    /**
     * Loads the resource.
     * 
     * @throws InvalidDescriptionException
     * @throws InvalidPluginException
     * @since 1.0.0
     */
    public final void load() {
        PluginHandler.loadPlugin(name + ".jar");
    }

    /**
     * Resource type specific processes.
     * 
     * @throws Exception
     *             Invalid file.
     * @since 1.0.0
     */
    protected abstract void process() throws Exception;

    /**
     * Removes the resource.
     * 
     * @throws Exception
     *             Invalid file.
     * @since 1.0.0
     */
    public final void remove() throws Exception {
        PluginHandler.unloadPlugin(name + ".jar");
        PluginHandler.softdeletePlugin(name + ".jar");
    }

    /**
     * Validates new resource based on provided checksums (if any) and being a
     * valid Bukkit plugin.
     * 
     * @throws Exception
     *             Invalid JAR file.
     * @return boolean based on validity of resource.
     * @since 1.0.0
     */
    public final boolean valid() throws Exception {
        JarFile jar = new JarFile("plugins" + File.separator + name + ".jar");
        JarEntry entry = jar.getJarEntry("plugin.yml");
        jar.close();

        if (entry != null) {
            return true;
        }
        return false;
    }
}
