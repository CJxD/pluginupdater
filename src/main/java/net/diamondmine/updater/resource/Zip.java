package net.diamondmine.updater.resource;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Zip resource.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class Zip extends Resource {
    /**
     * Zip resource.
     * 
     * @param url
     *            URL of resource.
     * @param type
     *            Type of resource (jar, zip, or rar).
     * @param name
     *            Name of resource.
     * @throws Exception
     *             Invalid file.
     * @since 1.0.0
     */
    public Zip(final URL url, final String type, final String name) throws Exception {
        super(url, type, name);
    }

    @Override
    protected final void process() throws Exception {
        int buffer = 2048;
        String path = "plugins" + File.separator + name + ".zip";
        File file = new File(path);
        ZipFile zip = new ZipFile(file);

        try {
            Enumeration<? extends ZipEntry> zipFileEntries = zip.entries();

            while (zipFileEntries.hasMoreElements()) {
                ZipEntry entry = zipFileEntries.nextElement();
                String currentEntry = entry.getName();
                File destFile = new File("plugins", currentEntry);
                File destParent = destFile.getParentFile();
                destParent.mkdirs();

                if (!entry.isDirectory()) {
                    BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));
                    int currentByte;
                    byte[] data = new byte[buffer];

                    FileOutputStream fos = new FileOutputStream(destFile);
                    BufferedOutputStream dest = new BufferedOutputStream(fos, buffer);

                    while ((currentByte = is.read(data, 0, buffer)) != -1) {
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();
                }
            }
        } finally {
            zip.close();
            file.delete();
        }
    }
}
