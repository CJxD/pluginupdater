package net.diamondmine.updater.resource;

import java.net.URL;

/**
 * JAR resource.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class JAR extends Resource {
    /**
     * JAR resource.
     * 
     * @param url
     *            URL of resource.
     * @param type
     *            Type of resource (jar, zip, or rar).
     * @param name
     *            Name of resource.
     * @since 1.0.0
     */
    public JAR(final URL url, final String type, final String name) {
        super(url, type, name);
    }

    @Override
    protected final void process() {
        return;
    }
}
