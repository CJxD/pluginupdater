package net.diamondmine.updater.resource;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Class for unloading, loading, and soft-deleting plugins.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class PluginHandler {
    /**
     * Unloads a plugin by name.
     * 
     * @param pluginName
     *            The name of the plugin to unload.
     * @return boolean depending on success.
     * @since 1.0.0
     */
    public static boolean unloadPlugin(final String pluginName) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        SimplePluginManager spmanager = (SimplePluginManager) manager;
        Plugin plugin = manager.getPlugin(pluginName);

        if (plugin == null) {
            return true;
        }

        try {
            spmanager.disablePlugin(plugin);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /**
     * Loads a plugin by name.
     * 
     * @param pluginName
     *            The name of the plugin to load.
     * @return boolean depending on success.
     * @since 1.0.0
     */
    public static boolean loadPlugin(final String pluginName) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        File file = new File("plugins", pluginName + ".jar");

        if (!file.exists()) {
            return false;
        }

        Plugin plugin = manager.getPlugin(pluginName);

        if (!(plugin instanceof JavaPlugin)) {
            return false;
        } else if (!plugin.isEnabled()) {
            manager.enablePlugin(plugin);
        } else {
            return false;
        }

        manager.enablePlugin(plugin);

        return true;
    }

    /**
     * Soft-deletes (renames/moves) a plugin.
     * 
     * @param filename
     *            The file name of the plugin.
     * @throws Exception
     *             Plugin doesn't exist.
     * @since 1.0.0
     */
    public static void softdeletePlugin(final String filename) throws Exception {
        File pluginBackup = new File(filename + ".old");
        if (pluginBackup.exists()) {
            pluginBackup.delete();
        }

        new File(filename).renameTo(new File(filename + ".old"));
    }

    /**
     * Checks if a plugin exists.
     * 
     * @param filename
     *            The file name of the plugin.
     * @return boolean depending on existence.
     * @since 1.0.0
     */
    public static boolean pluginExists(final String filename) {
        File plugin = new File("plugins", filename);
        if (!plugin.exists()) {
            return false;
        }

        return true;
    }
}
