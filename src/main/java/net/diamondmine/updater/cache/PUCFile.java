package net.diamondmine.updater.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import net.diamondmine.updater.PluginUpdater;

/**
 * PUC file object.
 * 
 * @author Jon la Cour
 * @since 1.0.0
 */
public class PUCFile implements Serializable {
    private static final long serialVersionUID = 2749050440062346563L;
    public int time;
    public String plugin;

    /**
     * Constructor.
     * 
     * @param plugin
     *            Name of plugin.
     * @since 1.0.0
     */
    public PUCFile(final String plugin) {
        this.plugin = plugin;
        time = (int) System.currentTimeMillis() / 1000;
    }

    /**
     * @return Age of PUC file in seconds.
     * @since 1.0.0
     */
    public final int getAge() {
        return ((int) (System.currentTimeMillis() / 1000) - time);
    }

    /**
     * @return Raw time of PUC file.
     * @since 1.0.0
     */
    public final int getTime() {
        return time;
    }

    /**
     * Loads a PUC file.
     * 
     * @return PUC file object.
     * @since 1.0.0
     */
    public final PUCFile load() {
        try {
            String separator = File.separator;
            File cache = new File("plugins" + separator + "PluginUpdater" + separator + "cache" + separator + plugin + ".puc");
            if (cache.exists()) {
                FileInputStream puc = new FileInputStream("plugins" + separator + "PluginUpdater" + separator + "cache" + separator + plugin + ".puc");
                ObjectInputStream ois = new ObjectInputStream(puc);
                PUCFile pucFile = (PUCFile) ois.readObject();
                ois.close();
                puc.close();
                return pucFile;
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    /**
     * Saves a PUC file.
     * 
     * @since 1.0.0
     */
    public final void save() {
        try {
            String separator = File.separator;
            FileOutputStream puc = new FileOutputStream("plugins" + separator + "PluginUpdater" + separator + "cache" + separator + plugin + ".puc");
            ObjectOutputStream oos = new ObjectOutputStream(puc);
            oos.writeObject(this);
            oos.close();
        } catch (Exception e) {
            PluginUpdater.log("Error saving cache file for " + plugin + ".", "warning");
        }
    }
}
