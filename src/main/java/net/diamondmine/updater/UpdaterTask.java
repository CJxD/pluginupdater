package net.diamondmine.updater;

import java.security.SecureRandom;

import net.diamondmine.updater.cache.PUCFile;

import org.bukkit.plugin.Plugin;

/**
 * Automatic plugin updater task.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class UpdaterTask implements Runnable {
    private final PluginUpdater pluginUpdater;

    public UpdaterTask(final PluginUpdater instance) {
        pluginUpdater = instance;
    }

    /**
     * Run method.
     */
    @Override
    public final void run() {
        Plugin[] plugins = pluginUpdater.getServer().getPluginManager().getPlugins();
        SecureRandom random = new SecureRandom();
        Plugin plugin = plugins[random.nextInt(plugins.length)];

        boolean autoupdate = false;
        if (!pluginUpdater.config.isSet("plugins." + plugin.getName().toLowerCase() + ".autoupdate")) {
            try {
                if (pluginUpdater.config.getString("plugins." + plugin.getName().toLowerCase() + ".autoupdate").toLowerCase().equals("on")) {
                    autoupdate = true;
                }
            } catch (Exception e) {
                autoupdate = false;
            }
        }

        if (autoupdate) {
            boolean refreshCache = true;
            PUCFile cache = new PUCFile(plugin.getName());
            cache = cache.load();
            if (cache != null) {
                if (((System.currentTimeMillis() / 1000) - cache.getAge()) / 60 < pluginUpdater.config.getInt("options.autoupdate.cacheage")) {
                    refreshCache = false;
                }
            }

            if (!refreshCache) {
                plugin = plugins[random.nextInt(plugins.length)];
            }

            String url = "http://dev.bukkit.org/server-mods/" + plugin.getName().toLowerCase() + "/";

            if (pluginUpdater.config.contains("plugins." + plugin.getName().toLowerCase() + ".location")) {
                url = pluginUpdater.config.getString("plugins." + plugin.getName().toLowerCase() + ".location");
            }

            try {
                PluginUpdater.log("Running automatic plugin update for " + plugin.getName() + ".");
                Runnable updater = new Updater(pluginUpdater, url, null, true);
                new Thread(updater).start();
            } catch (Exception e) {
                PluginUpdater.log("Error running automatic plugin update for " + plugin.getName() + ": " + e.getMessage(), "severe");
            }
            PUCFile pucFile = new PUCFile(plugin.getName());
            pucFile.save();
        } else {
            pluginUpdater.getServer().getScheduler().scheduleAsyncDelayedTask(pluginUpdater, this);
        }
    }
}
