package net.diamondmine.updater;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;

import net.diamondmine.updater.cache.PUCFile;
import net.diamondmine.updater.debugger.PULFile;
import net.diamondmine.updater.resource.JAR;
import net.diamondmine.updater.resource.PluginHandler;
import net.diamondmine.updater.resource.Resource;
import net.diamondmine.updater.resource.Zip;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Update handler.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class Updater implements Runnable {
    private final PluginUpdater pluginUpdater;
    public URL source = null;
    public String url = null;
    public String type = null;
    public String version = null;
    public String title = null;
    public String plugin = null;
    public String host = null;
    public boolean error = true;
    public boolean known = false;
    public String message = "Unknown error.";
    public CommandSender sender = null;

    /**
     * Run method.
     */
    @Override
    public final void run() {
        PUCFile cache = new PUCFile(plugin);
        cache = cache.load();
        if (cache != null) {
            if (((System.currentTimeMillis() / 1000) - cache.getAge()) / 60 >= pluginUpdater.config.getInt("options.autoupdate.cacheage")) {
                error = false;
                message = plugin + " is up to date.";
                send(message);
                return;
            }
        }

        boolean uptodate = false;
        host = source.getHost();
        if (host.equalsIgnoreCase("dev.bukkit.org")) {
            String[] pathArray = source.getPath().split("/");
            if (pathArray[1].equalsIgnoreCase("server-mods") && (pathArray[2] != null || !pathArray[2].isEmpty())) {
                plugin = pathArray[2];
            } else {
                error = true;
                message = "Plugin has supported host with invalid URL in configuration.";
                send(message);
                return;
            }
            if (PluginHandler.pluginExists(plugin + ".jar")) {
                known = true;
            }

            try {
                Document files = Jsoup.connect("http://dev.bukkit.org/server-mods/" + plugin + "/files/").timeout(2000).get();
                Elements table = files.select("tr[class]");
                String latestFile = null;

                file: {
                    for (Element tr : table) {
                        String fileName = tr.select("td.col-filename").text().toLowerCase();
                        if (fileName.contains(".jar") || fileName.contains(".zip")) {
                            latestFile = tr.select("td.col-file a").attr("abs:href");
                        }

                        if (fileName.contains(".jar")) {
                            type = "JAR";
                            break file;
                        } else if (fileName.contains(".zip")) {
                            type = "Zip";
                            break file;
                        } else {
                            type = "invalid";
                            break file;
                        }
                    }
                }

                pluginUpdater.insertOption(plugin, "type", type);

                if (type == "invalid") {
                    error = true;
                    message = "Plugin does not have any supported filetypes currently.";
                    send(message);
                    return;
                }

                plugin = files.select("h1").get(1).text();
                files.empty();

                Document latest = Jsoup.connect(latestFile).timeout(2000).get();
                Elements meta = latest.select("dd");
                version = meta.get(1).text();
                url = meta.get(2).select("a").attr("abs:href");
                String size = meta.get(3).text();
                String md5 = meta.get(4).text();
                title = latest.select("h1").get(2).text().replaceAll(plugin + " ", "");

                if (known && type == "JAR") {
                    uptodate = getMD5Checksum("plugins/" + plugin + ".jar").trim().equalsIgnoreCase(md5);
                } else if (known && type == "ZIP") {
                    uptodate = pluginUpdater.getZipMD5(plugin, "md5").trim().equalsIgnoreCase(md5);
                    pluginUpdater.insertOption(plugin, "md5", md5);
                }

                PUCFile pucFile = new PUCFile(plugin);
                pucFile.save();
                if (known && uptodate) {
                    error = false;
                    message = plugin + " is up to date.";
                    send(message);
                    return;
                } else {
                    if (!known) {
                        Resource resource = null;
                        if (type.equals("Zip")) {
                            resource = new Zip(new URL(url), type, plugin);
                        } else {
                            resource = new JAR(new URL(url), type, plugin);
                        }

                        resource.download();
                        if (resource.success) {
                            resource.load();
                        } else {
                            error = true;
                            message = plugin + " (" + title + ") was downloaded, but the " + type + " was invalid so it was not loaded.";
                            send(message);
                            return;
                        }

                        message = plugin + " (" + title + ") was installed. The " + type + " file was " + size + " in size.";
                        send(message);
                    } else {
                        Resource resource = null;
                        if (type.equals("Zip")) {
                            resource = new Zip(new URL(url), type, plugin);
                        } else {
                            resource = new JAR(new URL(url), type, plugin);
                        }

                        resource.remove();
                        resource.download();
                        if (resource.success) {
                            resource.load();
                        } else {
                            error = true;
                            message = plugin + " (" + title + ") was downloaded, but the " + type + " was invalid so it was not loaded.";
                            send(message);
                            return;
                        }

                        message = plugin + " was updated to " + title + ". The " + type + " file was " + size + " in size.";
                        send(message);
                    }
                    error = false;
                    return;
                }
            } catch (Exception e) {
                error = true;
                message = "Error updating/installing " + plugin + " from plugin host " + this.host + ", make sure you aren't missing dependencies.";
                send(message);
                PULFile log = new PULFile(e.getStackTrace(), e.getMessage());
                log.save();
                return;
            }
        } else {
            error = true;
            message = "Unsupported plugin host " + host + ".";
            send(message);
            return;
        }
    }

    /**
     * Updater for plugins.
     * 
     * @param pluginUpdater
     *            PluginUpdater class.
     * @param source
     *            URL of plugin source.
     * @param sender
     *            CommandSender for responses.
     * @param known
     *            Whether or not the plugin is already installed.
     */
    public Updater(final PluginUpdater pluginUpdater, final URL source, final CommandSender sender, final boolean known) {
        this.pluginUpdater = pluginUpdater;
        this.source = source;
        this.sender = sender;
        this.known = known;
    }

    /**
     * Updater for plugins.
     * 
     * @param pluginUpdater
     *            PluginUpdater class.
     * @param source
     *            String URL of plugin source.
     * @param sender
     *            CommandSender for responses.
     * @param known
     *            Whether or not the plugin is already installed.
     * @throws MalformedURLException
     *             Incorrect URL.
     */
    public Updater(final PluginUpdater pluginUpdater, final String source, final CommandSender sender, final boolean known) throws MalformedURLException {
        this(pluginUpdater, new URL(source), sender, known);
    }

    /**
     * Creates a checksum.
     * 
     * @param filename
     *            Filename or path to file.
     * @return MD5 string.
     * @throws Exception
     *             Invalid file.
     * @since 1.0.0
     */
    private byte[] createChecksum(final String filename) throws Exception {
        InputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    /**
     * Gets a file's MD5 checksum.
     * 
     * @param filename
     *            Filename or path to file.
     * @return MD5 string.
     * @throws Exception
     *             Invalid file.
     * @since 1.0.0
     */
    private String getMD5Checksum(final String filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";

        for (byte element : b) {
            result += Integer.toString((element & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    /**
     * Sends the command sender a message.
     * 
     * @param message
     *            Message to send to CommandSender.
     * @since 1.0.0
     */
    private void send(final String message) {
        if (sender == null) {
            PluginUpdater.log(message);
        } else {
            sender.sendMessage(ChatColor.RED + message);
        }
    }
}
